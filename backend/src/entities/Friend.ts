import { Field, Int, ObjectType } from 'type-graphql'

@ObjectType()
export class Friend {
  @Field() 
  _id: string
  
  @Field(() => Int) 
  age: number

  @Field(() => Int)
  index: number

  @Field() 
  company: string

  @Field()
  email: string
  
  @Field() 
  eyeColor: string

  @Field() 
  name: string

  @Field() 
  phone: string

  @Field() 
  picture: string
}
