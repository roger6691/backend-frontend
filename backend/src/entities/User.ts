import { Field, Int, ObjectType } from 'type-graphql'

import { Friend } from './Friend'

@ObjectType()
export class User {
  @Field({ nullable: false }) 
  _id: string

  @Field(() => Int) 
  index: number

  @Field(() => Int)
  age: number

  @Field(() => [Friend])
  friends: Friend[]

  @Field()
  company: string

  @Field()
  eyeColor: string

  @Field()
  greeting: string
  
  @Field()
  name: string

  @Field()
  phone: string

  @Field({ nullable: false }) 
  email: string
  
  @Field({ nullable: false }) 
  picture: string
}
