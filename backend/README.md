# ATENÇÃO: Utilize o YARN para instalar os pacotes em vez do NPM:

# Não use o NPM porque o package "coa" está problemático:

# Adicionado o trecho "resolutions": {"coa": "2.0.2"} ao package.json para o Yarn install resolver com versão do pacote que funciona

Execute "yarn install" e "yarn start" para rodar aplicação graphql. <br/>

Url do Graphql é http://localhost:4000/graphql
